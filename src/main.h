#ifndef MPVRENDERER_H_
#define MPVRENDERER_H_

#include <QWidget>

#include <mpv/client.h>
#include <mpv/render_gl.h>
#include "qthelper.h"

class MpvObject : public QWidget
{
    Q_OBJECT
    mpv_handle *mpv;

public:
    MpvObject(QWidget *parent = 0);
    virtual ~MpvObject();

    void play();

public Q_SLOTS:
    void move(int x, int y);
    void resize(int w, int h);
    void eventHandler();
    void command(const QVariant& params);
    void setProperty(const QString& name, const QVariant& value);
    QVariant getProperty(const QString& name);

Q_SIGNALS:
    void mpvEvents();
    void fileLoaded();
};

#endif
