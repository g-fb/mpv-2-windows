#include "main.h"

#include <stdexcept>
#include <clocale>

#include <QObject>
#include <QtGlobal>
#include <QOpenGLContext>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>
#include <QWidget>

static void wakeup(void *ctx)
{
    // This callback is invoked from any mpv thread (but possibly also
    // recursively from a thread that is calling the mpv API). Just notify
    // the Qt GUI thread to wake up (so that it can process events with
    // mpv_wait_event()), and return as quickly as possible.
    MpvObject *mainwindow = (MpvObject *)ctx;
    Q_EMIT mainwindow->mpvEvents();
}

MpvObject::MpvObject(QWidget * parent)
    : QWidget(parent)
{
    setWindowFlag(Qt::WindowStaysOnTopHint);

    mpv = mpv_create();
    if (!mpv)
        throw std::runtime_error("could not create mpv context");

    setAttribute(Qt::WA_DontCreateNativeAncestors);
    setAttribute(Qt::WA_NativeWindow);
    // If you have a HWND, use: int64_t wid = (intptr_t)hwnd;
    int64_t wid = winId();
    mpv_set_option(mpv, "wid", MPV_FORMAT_INT64, &wid);

//    mpv_set_option_string(mpv, "terminal", "yes");
//    mpv_set_option_string(mpv, "msg-level", "all=v");

    connect(this, &MpvObject::mpvEvents, this, &MpvObject::eventHandler, Qt::QueuedConnection);
    mpv_set_wakeup_callback(mpv, wakeup, this);

    if (mpv_initialize(mpv) < 0)
        throw std::runtime_error("could not initialize mpv context");
}

MpvObject::~MpvObject()
{
    mpv_terminate_destroy(mpv);
}

void MpvObject::eventHandler()
{
    while (mpv) {
        mpv_event *event = mpv_wait_event(mpv, 0);
        if (event->event_id == MPV_EVENT_NONE) {
            break;
        }
        switch (event->event_id) {
        case MPV_EVENT_FILE_LOADED: {
            Q_EMIT fileLoaded();
            break;
        }
        default: ;
        }
    }
}

void MpvObject::command(const QVariant& params)
{
    mpv::qt::command(mpv, params);
}

void MpvObject::setProperty(const QString& name, const QVariant& value)
{
    mpv::qt::set_property(mpv, name, value);
}

QVariant MpvObject::getProperty(const QString &name)
{
    return mpv::qt::get_property(mpv, name);
}

void MpvObject::move(int x, int y)
{
    QWidget::move(x, y);
}

void MpvObject::resize(int w, int h)
{
    QWidget::resize(w, h);
}

void MpvObject::play()
{
    QString filename = QStandardPaths::writableLocation(QStandardPaths::MoviesLocation).append("/test.mp4");
    const QByteArray c_filename = filename.toUtf8();
    const char *args[] = {"loadfile", c_filename.data(), NULL};
    mpv_command_async(mpv, 0, args);
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Qt sets the locale in the QGuiApplication constructor, but libmpv
    // requires the LC_NUMERIC category to be set to "C", so change it back.
    std::setlocale(LC_NUMERIC, "C");

    MpvObject mpv;
    mpv.setWindowFlag(Qt::FramelessWindowHint);
    mpv.show();
    mpv.play();

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    engine.rootContext()->setContextProperty("MpvWindow", &mpv);
    engine.load(url);


    return app.exec();
}
