import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Window 2.0

import org.kde.kirigami 2.11 as Kirigami

ApplicationWindow {
    id: root

    width: 1280
    height: 720
    visible: true

    onXChanged: MpvWindow.move(x, y)
    onYChanged: MpvWindow.move(x, y)
    onWidthChanged: MpvWindow.resize(width, height)
    onHeightChanged: MpvWindow.resize(width, height)
    onClosing: MpvWindow.close()

    Component.onCompleted: MpvWindow.resize(width, height)
}
